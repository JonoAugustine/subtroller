const { ControllerGroup, Controller } = require("subtroller");

const publicCtrlr = new Controller().make("get", "one", (req, res) => {
  res.send("Normal Route");
});

const privateCtrlr = new Controller().make("get", "one", (req, res) => {
  res.send("Super Secret Route");
});

module.exports = new ControllerGroup(publicCtrlr, privateCtrlr);
