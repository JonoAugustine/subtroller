const ctrlr = require("./controller");
const express = require("express");
const router = express.Router();

router.get("/", ctrlr.public.get.one);

router.get("/private", ctrlr.private.get.one);

module.exports = router;
