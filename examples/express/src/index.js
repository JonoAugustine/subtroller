const express = require("express");
const routes = require("./routes");

const server = express();

// use the router
server.use("/", routes);

const port = process.env.PORT || 4200;
server.listen(port, () => console.log(`Listening at localhost:${port}`));
