# Subtroller

A framework for making semantic route controllers. Using Subtroller, your routing
file will look like:

```javascript
router.route("/bees").get(beeCtrl.public.get.many);

router.route("/bees/:beeID").get(beeCtrl.private.get.bee);
```

## Install

```nolang
$ npm i --save subtroller
```

---

## Basic Usage | (Examples in the /examples/ folder)

Using subtroller is easy, the most simple way to use it is by creating a
number of subtrollers and adding them to a single controller:

```javascript
// mycontroller.js //
const { Subtroller, Controller, Methods } = require("subtroller");

// Make a subtroller for getting many resources
const getMany = new Subtroller("get", "many", () => {
  // Do some stuff with whatever framework you're using
  console.log("GET MANY");
});

// Make a subtroller for adding 1 resource
const postOne = new Subtroller(Methods.POST, "one", () => {
  console.log("POST ONE");
});

// Add the subtrollers to a controller
const ctrlr = new Controller().add(getMany, postOne);

// Export the controller
module.exports = ctrlr;
```

Now that we've built the Controller, we can get the handler functions easily:

```javascript
const ctrlr = require("./mycontroller.js");

ctrlr.post.one();
// $ POST ONE

ctrlr.get.many();
// $ GET MANY
```

If you don't want to use the `new Subtroller(...)` syntax, you can chain
`Controller.make(...)` calls instead:

```javascript
const ctrlr = new Controller()
                .make("get", "many", () => {...})
                .make("post" "one", () => {...})
```

## Using ControllerGroup

Sometimes it makes sense to group private and public route controllers
for the same endpoint togther. Subtroller makes it simple by giving another
layer on top using the `ControllerGroup` class:

```javascript
const { ControllerGroup, Controller } = require("subtroller");

// Make a public controller and give it a subtroller
const publicCtrlr = new Controller().make("get", "one", () => {...})

// Make a private controller
const privateCtrlr = new Controller().make("delete", "one" , () => {...})

// Add both to a Controller Group
const group = new ControllerGroup(publicCtrlr, privateCtrlr)
```

Now we can make sure we are using secured controllers where we need to:

```javascript
group.public.get.one();
group.private.delete.one();
```
