const { Subtroller } = require("../src");

describe("Subtroller", () => {
  describe("Constructor", () => {
    it("should not allow missing method", () => {
      expect(() => new Subtroller(undefined, "name", () => {})).toThrowError(
        /Invalid parameter type/i
      );
    });

    it("should not allow empty method", () => {
      expect(() => new Subtroller("", "name", () => {})).toThrowError(
        /1 char/i
      );
    });

    it("should not allow non-string method", () => {
      expect(() => new Subtroller(true, "name", () => {})).toThrowError(
        /must be a string/i
      );
    });

    it("should not allow missing name", () => {
      expect(() => new Subtroller("get", undefined, () => {})).toThrowError(
        /Invalid parameter type/i
      );
    });

    it("should not allow non-string name", () => {
      expect(() => new Subtroller("get", true, () => {})).toThrowError(
        /must be a string/i
      );
    });

    it("should not allow empty name", () => {
      expect(() => new Subtroller("get", "", () => {})).toThrowError(/1 char/i);
    });

    it("should not allow missing troller", () => {
      expect(() => new Subtroller("get", "name", undefined)).toThrowError(
        /Invalid parameter type/i
      );
    });

    it("should not allow non-function troller", () => {
      expect(() => new Subtroller("get", "name", true)).toThrowError(
        /must be a function/i
      );
    });

    it("should allow valid parameters", () => {
      expect(() => new Subtroller("get", "name", () => {})).not.toThrow();
    });
  });

  describe("Properties", () => {
    /** @type {Subtroller} */
    let subtroller;

    it("should register given method", () => {
      const m = "asdfghjASl";
      const s = new Subtroller(m, "name", () => {});
      expect(s.method === m);
    });

    it("should register given name", () => {
      const n = "asdfghjASl";
      const s = new Subtroller("get", n, () => {});
      expect(s.name === n);
    });

    it("should register given troller", () => {
      const s = new Subtroller("get", "name", () => 98);
      expect(s.troller() === 98);
    });
  });
});
