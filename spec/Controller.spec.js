const { Controller, Subtroller } = require("../src");

describe("Controller", () => {
  it("should retain all passed subtrollers", () => {
    const subs = [
      new Subtroller("post", "name", () => 0),
      new Subtroller("post", "name_2", () => 1)
    ];

    const c = new Controller(...subs);

    let i = 0;
    for (const sk in c.post) {
      expect(c.post[sk]() === i++);
    }
  });
});
