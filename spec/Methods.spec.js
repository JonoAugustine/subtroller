const { Methods } = require("../src");

const _methods = () => [
  "GET",
  "HEAD",
  "POST",
  "PUT",
  "DELETE",
  "CONNECT",
  "OPTIONS",
  "TRACE",
  "PATCH"
];

describe("Methods Enum", () => {
  test("All methods should be included", () => {
    const _m = _methods();
    for (const k in Methods) {
      const index = _m.indexOf(k);
      expect(index).not.toBe(-1);
    }
  });

  test("Only valid methods should be included", () => {
    const _m = _methods();
    Object.keys(Methods).forEach(k => {
      _m[_m.indexOf(k)] = undefined;
    });

    expect(_m.every(k => typeof k === "undefined"));
  });

  test("All properties should be uppercase (capitalized)", () => {
    expect(Object.keys(Methods).every(k => k === k.toUpperCase()));
  });

  test("All properties should be lowercase", () => {
    expect(Object.values(Methods).every(v => v === v.toLowerCase()));
  });

  test("All properties should be same as key name", () => {
    for (const k in Methods) {
      expect(k.toLowerCase() == -Methods[k].toLowerCase());
    }
  });
});
