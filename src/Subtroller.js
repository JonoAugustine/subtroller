/**
 * A Route Method Handler/Controller.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
class Subtroller {
  /**
   * All parameters are required.
   *
   * @param {string} method - The method name (get, put, etc.). Will be forced to lowercase.
   * @param {string} name - The name of the subtroller (one, many, bee, etc.)
   * @param {function} troller - The handler function.
   *
   * @since 0.1.0
   * @author Jonathan Augustine
   */
  constructor(method, name, troller) {
    Subtroller.validate({ method, name, troller });
    this.method = method.toLowerCase();
    this.name = name;
    this.troller = troller;
  }

  /**
   * Validates constructor parameters.
   *
   * @param {{method, name, troller}} props
   *
   * @since 0.1.0
   * @author Jonathan Augustine
   */
  static validate(props) {
    // Ensure all present
    for (const k in props) {
      if (typeof props[k] === "undefined") {
        throw new TypeError(
          `Invalid parameter type for "${k}" (was ${typeof props[k]})})`
        );
      }
    }

    // Validate Method
    if (typeof props.method !== "string") {
      throw new TypeError(
        `"method" must be a string (was ${typeof props.method})`
      );
    } else if (props.method.length < 1) {
      throw new TypeError('"method" must be at least 1 char');
    }

    // Validate Name
    if (typeof props.name !== "string") {
      throw new TypeError(`"name" must be a string (was ${typeof props.name})`);
    } else if (props.name.length < 1) {
      throw new TypeError('"name" must be at least 1 char');
    }

    // Validate troller
    if (typeof props.troller !== "function") {
      throw new TypeError(
        `"troller" must be a function (was ${typeof props.troller})`
      );
    }
  }
}

module.exports = Subtroller;
