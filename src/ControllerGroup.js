const Controller = require("./Controller");

/**
 * Route Controller Private-Public Grouping. Not needed but allows
 * for this syntax:
 * ```javascript
 * ctrl.public.etc
 * ctrl.private.etc
 * ```
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
class ControllerGroup {
  /**
   *
   * @param {Controller} open Controller for public routes.
   * @param {Controller} authorized Controler for private routes.
   *
   * @since 0.1.0
   * @author Jonathan Augustine
   */
  constructor(open, authorized) {
    /**
     * Controller for public routes.
     *
     * @type {Controller}
     */
    this.public = open;
    /**
     * Controller for private routes.
     *
     * @type {Controller}
     */
    this.private = authorized;
  }
}

module.exports = ControllerGroup;
