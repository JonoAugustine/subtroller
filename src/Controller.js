const Subtroller = require("./Subtroller");

/**
 * A Controller for a Route. The Controller houses Subtrollers for each
 * HTTP method.
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
class Controller {
  constructor() {
    /** GET Subtrollers */
    this.get = {};
    /** POST Subtrollers */
    this.post = {};
    /** PUT Subtrollers */
    this.put = {};
    /** DELETE Subtrollers */
    this.delete = {};
  }

  /**
   * Add a Subtroller to the Controller.
   *
   * @param {...Subtroller} subtrollers - Subtrollers to add
   * @returns {Controller} This Controller
   *
   * @since 0.1.0
   * @author Jonathan Augustine
   */
  add(...subtrollers) {
    subtrollers.forEach(subtroller => {
      // Ensure method obj exists
      if (!this[subtroller.method]) {
        this[subtroller.method] = {};
      }
      // Save subtroller to this Controller
      this[subtroller.method][subtroller.name] = subtroller.troller;
    });
    // Return this to allow chaining
    return this;
  }

  /**
   * Build a new Subtroller and add it to the Controller.
   *
   * @param {string} method - Subtroller method
   * @param {string} name - Subtroller name
   * @param {function} troller - Function of the subtroller
   * @returns {Controller} This Controller.
   *
   * @since 0.1.0
   * @author Jonathan Augustine
   */
  make(method, name, troller) {
    const subtroller = new Subtroller(method, name, troller);
    return this.add(subtroller);
  }
}

module.exports = Controller;
