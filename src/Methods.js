/**
 * HTTP Request Method Enum.
 *
 * @type {{GET: "get", HEAD: "head", POST: "post", PUT: "put", DELETE: "delete", CONNECT: "connect", OPTIONS: "options", TRACE: "trace", PATCH: "patch"}}
 *
 * @since 0.1.0
 * @author Jonathan Augustine
 */
const Methods = {
  GET: "get",
  HEAD: "head",
  POST: "post",
  PUT: "put",
  DELETE: "delete",
  CONNECT: "connect",
  OPTIONS: "options",
  TRACE: "trace",
  PATCH: "patch"
};

// Make it an "enums"
Object.freeze(Methods);

module.exports = Methods;
